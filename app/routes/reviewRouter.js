//Khai báo thư viện express
const express = require("express");

//Khai báo middleware
const {
    getAllReviewMidlleware,
    getReviewMidlleware,
    postReviewMidlleware,
    putReviewMidlleware,
    deleteReviewMidlleware
} = require (`../middleware/reviewMiddleware`);

// Tạo router
const reviewRouter = express.Router();

//Sử dụng Router
reviewRouter.get("/Review", getAllReviewMidlleware, (req,res)=>{
    res.json({
        message: "get all review"
    })
})

reviewRouter.get("/Review/:ReviewId", getReviewMidlleware, (req,res)=>{
    let ReviewId = req.params.ReviewId
    res.json({
        message: `ReviewId = ${ReviewId}`
    })
})

reviewRouter.post("/Review",postReviewMidlleware,(req,res)=>{
    res.json ({
        message: "Post Review"
    })
})

reviewRouter.put("/Review/:ReviewId",putReviewMidlleware,(req,res)=>{
    let ReviewId = req.params.ReviewId
    res.json({
        message: `ReviewId = ${ReviewId}`
    })
})

reviewRouter.delete("/Review/:ReviewId", deleteReviewMidlleware,(req,res)=>{
    let ReviewId = req.params.ReviewId
    res.json({
        message: `ReviewId = ${ReviewId}`
    })
})

module.exports = {reviewRouter}